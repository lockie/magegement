(local window
       (am.window
        {:title "Magegement"
         :width 1024
         :height 600
         :clear_color (vec4 0 0 0 1)}))

(local intro (require :intro))
(local game (require :game))
(local credits (require :credits))

(math.randomseed (os.time))
(intro window game credits)

VERSION=0.1.0

SOURCES := $(wildcard *.fnl)
COMPILED := $(patsubst %.fnl,%.lua,$(SOURCES)) fonts.lua

all: $(COMPILED)

%.lua: %.fnl
	fennel --compile --correlate $< > $@

fonts.lua: $(wildcard fonts/*.ttf)
	amulet pack -png fonts.png -lua fonts.lua $(addsuffix @18,$^)

release: release/magegement-$(VERSION)-html.zip

release/magegement-$(VERSION)-html.zip: $(COMPILED)
	mkdir -p release
	amulet export -r -html -nozipdir -d release

upload: release/magegement-$(VERSION)-html.zip
	butler push $^ awkravchuk/magegement:html --userversion $(VERSION)

count:
	cloc *.fnl --force-lang=clojure

clean:
	rm -rf $(COMPILED) release

.PHONY: count clean upload

return {
  version = "1.2",
  luaversion = "5.1",
  tiledversion = "1.3.3",
  orientation = "orthogonal",
  renderorder = "right-down",
  width = 32,
  height = 18,
  tilewidth = 16,
  tileheight = 16,
  nextlayerid = 4,
  nextobjectid = 6,
  properties = {},
  tilesets = {
    {
      name = "Floor",
      firstgid = 1,
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      columns = 21,
      image = "../images/Floor.png",
      imagewidth = 336,
      imageheight = 624,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 16,
        height = 16
      },
      properties = {},
      terrains = {},
      tilecount = 819,
      tiles = {}
    },
    {
      name = "Wall",
      firstgid = 820,
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      columns = 20,
      image = "../images/Wall.png",
      imagewidth = 320,
      imageheight = 816,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 16,
        height = 16
      },
      properties = {},
      terrains = {},
      tilecount = 1020,
      tiles = {
        {
          id = 0,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 1,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 2,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 3,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 4,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 5,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 6,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 7,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 8,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 9,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 10,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 11,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 12,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 13,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 14,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 15,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 16,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 17,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 18,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 19,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 20,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 21,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 22,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 23,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 24,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 25,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 26,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 27,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 28,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 29,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 30,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 31,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 32,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 33,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 34,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 35,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 36,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 37,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 38,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 39,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 40,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 41,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 42,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 43,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 44,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 45,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 46,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 47,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 48,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 49,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 50,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 51,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 52,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 53,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 54,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 55,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 56,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 57,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 58,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 59,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 60,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 61,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 62,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 63,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 64,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 65,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 66,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 67,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 68,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 69,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 70,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 71,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 72,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 73,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 74,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 75,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 76,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 77,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 78,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 79,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 80,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 81,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 82,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 83,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 84,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 85,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 86,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 87,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 88,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 89,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 90,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 91,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 92,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 93,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 94,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 95,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 96,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 97,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 98,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 99,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 100,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 101,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 102,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 103,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 104,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 105,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 106,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 107,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 108,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 109,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 110,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 111,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 112,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 113,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 114,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 115,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 116,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 117,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 118,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 119,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 120,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 121,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 122,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 123,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 124,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 125,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 126,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 127,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 128,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 129,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 130,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 131,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 132,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 133,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 134,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 135,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 136,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 137,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 138,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 139,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 140,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 141,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 142,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 143,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 144,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 145,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 146,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 147,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 148,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 149,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 150,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 151,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 152,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 153,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 154,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 155,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 156,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 157,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 158,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 159,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 160,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 161,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 162,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 163,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 164,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 165,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 166,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 167,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 168,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 169,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 170,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 171,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 172,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 173,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 174,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 175,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 176,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 177,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 178,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 179,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 180,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 181,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 182,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 183,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 184,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 185,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 186,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 187,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 188,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 189,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 190,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 191,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 192,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 193,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 194,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 195,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 196,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 197,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 198,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 199,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 200,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 201,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 202,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 203,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 204,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 205,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 206,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 207,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 208,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 209,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 210,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 211,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 212,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 213,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 214,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 215,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 216,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 217,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 218,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 219,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 220,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 221,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 222,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 223,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 224,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 225,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 226,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 227,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 228,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 229,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 230,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 231,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 232,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 233,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 234,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 235,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 236,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 237,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 238,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 239,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 240,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 241,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 242,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 243,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 244,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 245,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 246,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 247,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 248,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 249,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 250,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 251,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 252,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 253,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 254,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 255,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 256,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 257,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 258,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 259,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 260,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 261,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 262,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 263,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 264,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 265,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 266,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 267,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 268,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 269,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 270,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 271,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 272,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 273,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 274,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 275,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 276,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 277,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 278,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 279,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 280,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 281,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 282,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 283,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 284,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 285,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 286,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 287,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 288,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 289,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 290,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 291,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 292,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 293,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 294,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 295,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 296,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 297,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 298,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 299,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 300,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 301,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 302,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 303,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 304,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 305,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 306,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 307,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 308,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 309,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 310,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 311,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 312,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 313,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 314,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 315,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 316,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 317,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 318,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 319,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 320,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 321,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 322,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 323,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 324,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 325,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 326,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 327,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 328,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 329,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 330,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 331,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 332,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 333,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 334,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 335,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 336,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 337,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 338,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 339,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 340,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 341,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 342,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 343,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 344,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 345,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 346,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 347,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 348,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 349,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 350,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 351,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 352,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 353,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 354,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 355,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 356,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 357,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 358,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 359,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 360,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 361,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 362,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 363,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 364,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 365,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 366,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 367,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 368,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 369,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 370,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 371,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 372,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 373,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 374,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 375,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 376,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 377,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 378,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 379,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 380,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 381,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 382,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 383,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 384,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 385,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 386,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 387,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 388,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 389,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 390,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 391,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 392,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 393,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 394,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 395,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 396,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 397,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 398,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 399,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 400,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 401,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 402,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 403,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 404,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 405,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 406,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 407,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 408,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 409,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 410,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 411,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 412,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 413,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 414,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 415,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 416,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 417,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 418,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 419,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 420,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 421,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 422,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 423,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 424,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 425,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 426,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 427,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 428,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 429,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 430,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 431,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 432,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 433,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 434,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 435,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 436,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 437,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 438,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 439,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 440,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 441,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 442,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 443,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 444,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 445,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 446,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 447,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 448,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 449,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 450,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 451,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 452,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 453,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 454,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 455,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 456,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 457,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 458,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 459,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 460,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 461,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 462,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 463,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 464,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 465,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 466,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 467,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 468,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 469,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 470,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 471,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 472,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 473,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 474,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 475,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 476,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 477,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 478,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 479,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 480,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 481,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 482,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 483,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 484,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 485,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 486,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 487,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 488,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 489,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 490,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 491,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 492,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 493,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 494,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 495,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 496,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 497,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 498,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 499,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 500,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 501,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 502,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 503,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 504,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 505,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 506,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 507,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 508,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 509,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 510,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 511,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 512,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 513,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 514,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 515,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 516,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 517,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 518,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 519,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 520,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 521,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 522,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 523,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 524,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 525,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 526,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 527,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 528,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 529,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 530,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 531,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 532,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 533,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 534,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 535,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 536,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 537,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 538,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 539,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 540,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 541,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 542,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 543,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 544,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 545,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 546,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 547,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 548,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 549,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 550,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 551,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 552,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 553,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 554,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 555,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 556,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 557,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 558,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 559,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 560,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 561,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 562,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 563,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 564,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 565,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 566,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 567,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 568,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 569,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 570,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 571,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 572,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 573,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 574,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 575,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 576,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 577,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 578,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 579,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 580,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 581,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 582,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 583,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 584,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 585,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 586,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 587,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 588,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 589,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 590,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 591,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 592,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 593,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 594,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 595,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 596,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 597,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 598,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 599,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 600,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 601,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 602,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 603,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 604,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 605,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 606,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 607,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 608,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 609,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 610,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 611,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 612,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 613,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 614,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 615,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 616,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 617,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 618,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 619,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 620,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 621,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 622,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 623,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 624,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 625,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 626,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 627,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 628,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 629,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 630,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 631,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 632,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 633,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 634,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 635,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 636,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 637,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 638,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 639,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 640,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 641,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 642,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 643,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 644,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 645,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 646,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 647,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 648,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 649,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 650,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 651,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 652,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 653,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 654,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 655,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 656,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 657,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 658,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 659,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 660,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 661,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 662,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 663,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 664,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 665,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 666,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 667,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 668,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 669,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 670,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 671,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 672,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 673,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 674,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 675,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 676,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 677,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 678,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 679,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 680,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 681,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 682,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 683,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 684,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 685,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 686,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 687,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 688,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 689,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 690,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 691,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 692,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 693,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 694,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 695,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 696,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 697,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 698,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 699,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 700,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 701,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 702,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 703,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 704,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 705,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 706,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 707,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 708,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 709,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 710,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 711,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 712,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 713,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 714,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 715,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 716,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 717,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 718,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 719,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 720,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 721,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 722,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 723,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 724,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 725,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 726,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 727,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 728,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 729,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 730,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 731,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 732,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 733,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 734,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 735,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 736,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 737,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 738,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 739,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 740,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 741,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 742,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 743,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 744,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 745,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 746,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 747,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 748,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 749,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 750,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 751,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 752,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 753,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 754,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 755,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 756,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 757,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 758,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 759,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 760,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 761,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 762,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 763,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 764,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 765,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 766,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 767,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 768,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 769,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 770,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 771,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 772,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 773,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 774,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 775,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 776,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 777,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 778,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 779,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 780,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 781,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 782,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 783,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 784,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 785,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 786,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 787,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 788,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 789,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 790,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 791,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 792,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 793,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 794,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 795,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 796,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 797,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 798,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 799,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 800,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 801,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 802,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 803,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 804,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 805,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 806,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 807,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 808,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 809,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 810,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 811,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 812,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 813,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 814,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 815,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 816,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 817,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 818,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 819,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 820,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 821,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 822,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 823,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 824,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 825,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 826,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 827,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 828,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 829,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 830,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 831,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 832,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 833,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 834,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 835,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 836,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 837,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 838,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 839,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 840,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 841,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 842,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 843,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 844,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 845,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 846,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 847,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 848,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 849,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 850,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 851,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 852,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 853,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 854,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 855,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 856,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 857,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 858,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 859,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 860,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 861,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 862,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 863,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 864,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 865,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 866,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 867,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 868,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 869,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 870,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 871,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 872,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 873,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 874,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 875,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 876,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 877,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 878,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 879,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 880,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 881,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 882,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 883,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 884,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 885,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 886,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 887,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 888,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 889,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 890,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 891,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 892,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 893,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 894,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 895,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 896,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 897,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 898,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 899,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 900,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 901,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 902,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 903,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 904,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 905,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 906,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 907,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 908,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 909,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 910,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 911,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 912,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 913,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 914,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 915,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 916,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 917,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 918,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 919,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 920,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 921,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 922,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 923,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 924,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 925,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 926,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 927,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 928,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 929,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 930,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 931,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 932,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 933,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 934,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 935,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 936,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 937,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 938,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 939,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 940,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 941,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 942,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 943,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 944,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 945,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 946,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 947,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 948,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 949,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 950,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 951,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 952,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 953,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 954,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 955,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 956,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 957,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 958,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 959,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 960,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 961,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 962,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 963,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 964,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 965,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 966,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 967,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 968,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 969,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 970,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 971,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 972,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 973,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 974,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 975,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 976,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 977,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 978,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 979,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 980,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 981,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 982,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 983,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 984,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 985,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 986,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 987,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 988,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 989,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 990,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 991,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 992,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 993,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 994,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 995,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 996,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 997,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 998,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 999,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 1000,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 1001,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 1002,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 1003,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 1004,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 1005,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 1006,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 1007,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 1008,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 1009,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 1010,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 1011,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 1012,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 1013,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 1014,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 1015,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 1016,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 1017,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 1018,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 1019,
          properties = {
            ["obstacle"] = true
          }
        }
      }
    },
    {
      name = "Door0",
      firstgid = 1840,
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      columns = 8,
      image = "../images/Door0.png",
      imagewidth = 128,
      imageheight = 96,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 16,
        height = 16
      },
      properties = {},
      terrains = {},
      tilecount = 48,
      tiles = {
        {
          id = 41,
          properties = {
            ["exit"] = true
          }
        },
        {
          id = 43,
          properties = {
            ["obstacle"] = true
          }
        }
      }
    },
    {
      name = "Tile",
      firstgid = 1888,
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      columns = 8,
      image = "../images/Tile.png",
      imagewidth = 128,
      imageheight = 64,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 16,
        height = 16
      },
      properties = {},
      terrains = {},
      tilecount = 32,
      tiles = {
        {
          id = 8,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 9,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 10,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 11,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 12,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 13,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 14,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 15,
          properties = {
            ["obstacle"] = true
          }
        }
      }
    },
    {
      name = "Decor0",
      firstgid = 1920,
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      columns = 8,
      image = "../images/Decor0.png",
      imagewidth = 128,
      imageheight = 352,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 16,
        height = 16
      },
      properties = {},
      terrains = {},
      tilecount = 176,
      tiles = {
        {
          id = 32,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 33,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 34,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 35,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 36,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 37,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 38,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 39,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 40,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 41,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 42,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 43,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 44,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 45,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 46,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 47,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 48,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 49,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 50,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 51,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 52,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 53,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 54,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 55,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 56,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 57,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 58,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 59,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 60,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 61,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 62,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 63,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 80,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 81,
          properties = {
            ["obstacle"] = true
          }
        },
        {
          id = 82,
          properties = {
            ["obstacle"] = true
          }
        }
      }
    },
    {
      name = "Trap0",
      firstgid = 2096,
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      columns = 8,
      image = "../images/Trap0.png",
      imagewidth = 128,
      imageheight = 80,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 16,
        height = 16
      },
      properties = {},
      terrains = {},
      tilecount = 40,
      tiles = {
        {
          id = 18,
          properties = {
            ["hole"] = false
          }
        },
        {
          id = 19,
          properties = {
            ["hole"] = false
          }
        },
        {
          id = 20,
          properties = {
            ["hole"] = false
          }
        },
        {
          id = 21,
          properties = {
            ["hole"] = false
          }
        },
        {
          id = 24,
          properties = {
            ["trap"] = true
          }
        },
        {
          id = 25,
          properties = {
            ["trap"] = true
          }
        },
        {
          id = 26,
          properties = {
            ["trap"] = true
          }
        },
        {
          id = 27,
          properties = {
            ["trap"] = true
          }
        },
        {
          id = 28,
          properties = {
            ["trap"] = true
          }
        },
        {
          id = 29,
          properties = {
            ["trap"] = true
          }
        },
        {
          id = 30,
          properties = {
            ["trap"] = true
          }
        },
        {
          id = 31,
          properties = {
            ["trap"] = true
          }
        }
      }
    }
  },
  layers = {
    {
      type = "tilelayer",
      id = 1,
      name = "ground",
      x = 0,
      y = 0,
      width = 32,
      height = 18,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "lua",
      data = {
        831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831,
        831, 880, 881, 881, 881, 881, 881, 884, 881, 881, 881, 881, 881, 881, 881, 881, 881, 881, 881, 881, 881, 881, 881, 881, 881, 881, 881, 881, 881, 881, 882, 831,
        831, 900, 1883, 149, 149, 149, 149, 900, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 900, 831,
        831, 900, 149, 149, 149, 149, 149, 900, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 900, 831,
        831, 900, 149, 149, 149, 149, 149, 900, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 900, 831,
        831, 900, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 900, 831,
        831, 900, 149, 149, 149, 149, 149, 900, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 900, 831,
        831, 900, 149, 149, 149, 149, 149, 900, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 900, 831,
        831, 900, 149, 149, 149, 149, 149, 900, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 900, 831,
        831, 900, 149, 149, 149, 149, 149, 900, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 900, 831,
        831, 900, 149, 149, 149, 149, 149, 900, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 900, 831,
        831, 900, 149, 149, 149, 149, 149, 900, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 900, 831,
        831, 900, 149, 149, 149, 149, 149, 900, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 900, 831,
        831, 900, 149, 149, 149, 149, 149, 900, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 1897, 900, 831,
        831, 900, 149, 149, 149, 149, 149, 900, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 1881, 900, 831,
        831, 900, 149, 149, 149, 149, 149, 900, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 149, 900, 831,
        831, 920, 881, 881, 881, 881, 881, 924, 881, 881, 881, 881, 881, 881, 881, 881, 881, 881, 881, 881, 881, 881, 881, 881, 881, 881, 881, 881, 881, 881, 922, 831,
        831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831, 831
      }
    },
    {
      type = "tilelayer",
      id = 3,
      name = "decor",
      x = 0,
      y = 0,
      width = 32,
      height = 18,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 1986, 0, 0, 0, 0, 0, 1986, 0, 0, 0, 0, 1986, 0, 0, 0, 0, 1986, 0, 0, 0, 0, 1986, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1952, 1953, 1955, 1956, 1953, 1954, 1955, 1952, 1959, 0, 0, 0, 0, 2074, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1977, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2000, 2001, 2001, 2001, 2001, 2001, 2001, 2001, 2002, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 2123, 0, 0, 1846, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1942, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1942, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2019, 1942, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 1943, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 1943, 0, 0, 0, 0, 0, 0, 0, 2123, 0, 0, 0, 0, 2123, 0, 0, 0, 0, 2123, 0, 0, 0, 0, 2123, 0, 0, 0, 0, 0, 0,
        0, 0, 2018, 0, 0, 0, 2076, 0, 1937, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
      }
    },
    {
      type = "objectgroup",
      id = 2,
      name = "objects",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 1,
          name = "player",
          type = "player",
          shape = "point",
          x = 32,
          y = 48,
          width = 0,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 2,
          name = "zombie",
          type = "mob",
          shape = "point",
          x = 144,
          y = 80,
          width = 0,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {
            ["hp"] = 3
          }
        },
        {
          id = 4,
          name = "zombie",
          type = "mob",
          shape = "point",
          x = 448,
          y = 208,
          width = 0,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {
            ["hp"] = 3
          }
        },
        {
          id = 5,
          name = "zombie",
          type = "mob",
          shape = "point",
          x = 208,
          y = 176,
          width = 0,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {
            ["hp"] = 3
          }
        }
      }
    }
  }
}

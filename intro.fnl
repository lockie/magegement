(local fonts (require :fonts))

(fn intro [window start-game show-credits]
  (local text-color (vec4 1 1 1 1))
  (var intro-mode true)
  (set window.scene
       (: (^
           (am.group)
           [(^
             (am.translate 0 75)
             (am.text
              fonts.immortal18
              "MAGEGEMENT"
              text-color))
            (^
             (am.translate 0 -25)
             (am.text
              fonts.immortal18
              "To qualify as a master of Control Mage Order, you enter the training dungeon.
It is full of deadly traps and vile enemies, but fortunately you've mastered the Management Spell.
Good luck!"
              text-color))])
          :action
          (fn [scene]
            (when intro-mode
              (when (: window :mouse_pressed :left)
                (start-game window show-credits)
                (set intro-mode false)))))))

(local lume (require "contrib.lume"))

(local character (require :character))
(local mob (require :mob))
(local globals (require :globals))

(local hp-texture (am.texture2d (.. "images/hp.png")))
(local mana-texture (am.texture2d (.. "images/mana.png")))

(fn create [window map load-next-level data]
  (local base-char (character.create window map data))
  (var char base-char)
  (local char-scene-node (char.scene-node))
  (local control-range 5)
  (local control-mana-cost 2)
  (local max-control-time 6)
  (local max-mana 5)
  (var mana max-mana)
  (var control-target nil)
  (var attack-target nil)
  (var control-time 0)

  (let [tile-size globals.tile-size
        pixel-scale globals.pixel-scale
        scaled-tile-size (* pixel-scale tile-size)]

    (fn action [scene]
      (fn control [mob]
        (set control-time 0)
        (if mob
            (when (>= mana control-mana-cost)
              (set mana (- mana control-mana-cost))
              (: (: (mob.scene-node) :action :controller action) :append
                 (^
                  (am.blend :add_alpha)
                  (am.particles2d
                   {:source_pos_var (vec2 16 16)
                    :max_particles 100
                    :emission_rate 100
                    :start_particles 100
                    :life 0.2
                    :speed 0
                    :start_color (vec4 0.43 0.77 0.8 0.8)
                    :start_size 1
                    :start_size_var 3})))
              (: (: char-scene-node :action :controller (fn []))
                 :action (am.play "sounds/spell.ogg")))
            (do
              (when control-target
                (: (control-target.scene-node) :remove :particles2d)
                (control-target.set-target nil nil))
              (: char-scene-node :action :controller action)))
        (base-char.set-target nil nil)
        (set char (if mob (mob.character) base-char))
        (char.set-target nil nil)
        (set control-target mob)
        (set attack-target nil))

      ;; loose control when hit
      (base-char.set-hit-callback (fn [] (control nil)))

      ;; controlled mob death
      (when (and control-target (not (char.alive?)))
        (control nil))

      ;; control stop
      (when control-target
        (set control-time (+ control-time am.delta_time))
        (when (> control-time max-control-time)
          (control nil)))

      ;; controls
      (when (and (window:mouse_pressed :left) (base-char.alive?))
        (let [mouse-pos (window:mouse_position)
              target-x (- (/ (+ (/ window.width 2) mouse-pos.x) scaled-tile-size) 0.5)
              target-y (- (/ (- (/ window.height 2) mouse-pos.y) scaled-tile-size) 0.5)
              (x y) (char.position)
              mobs (lume.filter (mob.list) (fn [m] (and (m.alive?) (not (= m control-target)))))
              possible-target (lume.first
                               (lume.sort
                                mobs
                                (fn [m1 m2]
                                  (< (character.distance* m1 target-x target-y)
                                     (character.distance* m2 target-x target-y)))))]
          (if (and
               possible-target
               (< (character.distance* possible-target target-x target-y) 2))
              (if control-target
                  (set attack-target possible-target)
                  (if (< (character.distance possible-target char) control-range)
                      (control possible-target)
                      (char.set-target target-x target-y)))
              (char.set-target target-x target-y))))

      ;; attack as controlled mob
      (when (and control-target attack-target)
        (control-target.attack attack-target)
        (when (not (attack-target.alive?))
          (control-target.stop)
          (set attack-target nil)))

      ;; update hp and mana indicators
      (let [hp (base-char.hp)]
        (each [i child (: (window.scene :hp) :child_pairs)]
          (let [hp-heart (child :sprite)
                source hp-heart.source]
            (if (> i hp)
                (set hp-heart.source (lume.merge source {:s1 0.5 :s2 1}))
                (set hp-heart.source (lume.merge source {:s1 0 :s2 0.5}))))))
      (each [i child (: (window.scene :mana) :child_pairs)]
        (let [mana-heart (child :sprite)
              source mana-heart.source]
          (if (> i mana)
              (set mana-heart.source (lume.merge source {:s1 0.5 :s2 1}))
              (set mana-heart.source (lume.merge source {:s1 0 :s2 0.5})))))

      ;; go next level
      (let [(x y) (base-char.position)]
        (if (and (base-char.alive?) (map.exit? x y))
            (do
              (load-next-level)
              true)
            false)))

    (fn scene-node []
      (let [hp-hearts []
            mana-hearts []
            y (- 12 window.height)]
        (for [i 1 (base-char.max-hp)]
          (tset hp-hearts i
                (^ (am.translate (* tile-size i) y)
                   (am.sprite {:texture hp-texture :x1 0 :y1 0 :x2 tile-size :y2 tile-size
                               :width tile-size :height tile-size :s1 0 :t1 0 :s2 0.5 :t2 1}))))
        (for [i max-mana 1 -1]
          (tset mana-hearts i
                (^ (am.translate (- window.width (* tile-size i)) y)
                   (am.sprite {:texture mana-texture :x1 0 :y1 0 :x2 tile-size :y2 tile-size
                               :width tile-size :height tile-size :s1 0 :t1 0 :s2 0.5 :t2 1}))))
        (am.group
         [(: (am.group hp-hearts) :tag :hp)
          (: (am.group mana-hearts) :tag :mana)
          (: char-scene-node :action :controller action)])))

    {:scene-node scene-node
     :position base-char.position
     :hp base-char.hp
     :set-hp base-char.set-hp
     :mana (fn [] mana)
     :max-mana (fn [] max-mana)
     :set-mana (fn [new-mana] (set mana new-mana))
     :alive? base-char.alive?
     }))

{:create create
 }

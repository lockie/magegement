(local lume (require "contrib.lume"))

(local globals (require :globals))

(local blood-small-texture (am.texture2d (.. "images/blood-small.png")))
(local blood-large-texture (am.texture2d (.. "images/blood-large.png")))
(local empty-texture (am.texture2d 1))

(var counter 0)

(fn create [window map data]
  (var x (/ data.x globals.tile-size))
  (var y (/ data.y globals.tile-size))
  (var target-x nil)
  (var target-y nil)
  (var direction 0)
  (var frame 0)
  (var time-counter 0)
  (var hit-points (or data.hp 5))
  (var max-hit-points (or data.hp 5))
  (var trap-time 0)
  (var hit-callback (fn []))
  (local trap-reactivation-time 3)
  (local texture (am.texture2d (.. "images/" data.name ".png")))
  (local direction-count (/ texture.height globals.tile-size))
  (local frame-count (/ texture.width globals.tile-size))
  (local anim-speed 0.25)
  (local speed (or data.speed 1))
  (local margin 0.1)
  (local name (.. data.name (tostring counter)))
  (local floating (or data.floating false))
  (set counter (+ 1 counter))

  (fn moving? [] (not (= nil target-x)))

  (fn alive? [] (> hit-points 0))

  (let [tile-size globals.tile-size
        pixel-scale globals.pixel-scale
        scaled-tile-size (* pixel-scale tile-size)]

    (fn stop []
      (set target-x nil)
      (set target-y nil))

    (fn set-hp [new-hp]
      (hit-callback)
      (let [node (window.scene (.. name :sprite))]
        (if (and (> new-hp 0) (< new-hp hit-points))
            (node:action (am.play (.. "sounds/" data.name "-pain.ogg")))
            (< new-hp 0)
            (node:action (am.play (.. "sounds/" data.name "-death.ogg")))))
      (set hit-points new-hp)
      (when (<= hit-points 0)
        (stop)))

    (fn move-x []
      (set direction (if (> x target-x) 2 1))
      (let [new-x (+ x (* speed am.delta_time (lume.sign (- target-x x))))]
        (if (map.obstacle? new-x y)
            (stop)
            (set x new-x))))
    (fn move-y []
      (set direction (if (> y target-y) 0 3))
      (let [new-y (+ y (* speed am.delta_time (lume.sign (- target-y y))))]
        (if (map.obstacle? x new-y)
            (stop)
            (set y new-y))))
    (fn action [scene]
      ;; target reaching
      (when (and (alive?) (moving?))
        ;; movement
        (let [diff-x (math.abs (- x target-x))
              diff-y (math.abs (- y target-y))]
          (if (and (> diff-x margin) (> diff-y margin))
              (if (> (lume.round diff-x) (lume.round diff-y))
                  (move-x)
                  (move-y))
              (> diff-x margin)
              (move-x)
              (> diff-y margin)
              (move-y)
              (stop)))
        (set direction (lume.clamp direction 0 (- direction-count 1))))

      ;; fall in hole case
      (when (and (alive?) (not floating) (map.hole? x y))
        (set hit-points 0)
        (let [node (window.scene (.. name :sprite))]
          (node:action (am.play (.. "sounds/scream.ogg")))))

      ;; beware it's a trap
      (when (not (= trap-time 0))
        (set trap-time (+ trap-time am.delta_time))
        (when (> trap-time trap-reactivation-time)
          (set trap-time 0)))
      (when (and (alive?) (map.trap? x y) (= trap-time 0))
        (set trap-time am.delta_time)
        (set-hp (- hit-points 4))
        (let [node (window.scene (.. name :sprite))]
          (node:action (am.play (.. "sounds/trap.ogg")))))

      ;; update state
      (set time-counter (+ time-counter am.delta_time))
      (when (> time-counter anim-speed)
        (set time-counter (% time-counter anim-speed))
        (if (moving?)
            (do
              (set frame (+ 1 frame))
              (when (> frame (- frame-count 1))
                (set frame 0)))
            (set frame 0)))

      ;; draw
      (let [pos (scene (.. name :pos))]
        (set pos.x (* scaled-tile-size (+ 0.5 x)))
        (set pos.y (- (* scaled-tile-size (+ 0.5 y)))))
      (let [sprite (scene (.. name :sprite))
            source sprite.source]
        (set sprite.source
             (lume.merge
              source
              {:texture
               (if (alive?)
                   source.texture
                   (= hit-points 0)
                   empty-texture
                   (> hit-points (- max-hit-points))
                   blood-small-texture
                   blood-large-texture)
               :s1 (if (alive?) (/ frame frame-count) 0)
               :t1 (if (alive?) (/ direction direction-count) 0)
               :s2 (if (alive?) (/ (+ frame 1) frame-count) 1)
               :t2 (if (alive?) (/ (+ direction 1) direction-count) 1)})))
      false)

    (fn scene-node []
      (:
       (^
        (: (am.translate
            (* scaled-tile-size (+ 0.5 x))
            (- (* scaled-tile-size (+ 0.5 y)))) :tag (.. name :pos))
        (am.scale pixel-scale)
        (: (am.sprite {:texture texture :x1 0 :y1 0 :x2 tile-size :y2 tile-size
                       :width tile-size :height tile-size :s1 0 :t1 0 :s2 1 :t2 1})
           :tag (.. name :sprite)))
       :action action))

    (fn set-target [x y]
      (set target-x x)
      (set target-y y))

    {:stop stop
     :scene-node scene-node
     :position (fn [] (values x y))
     :set-target set-target
     :hp (fn [] hit-points)
     :set-hp set-hp
     :max-hp (fn [] max-hit-points)
     :alive? alive?
     :set-hit-callback (fn [callback] (set hit-callback callback))
     }))

(fn distance [c1 c2]
  (let [(x1 y1) (c1.position)
        (x2 y2) (c2.position)]
    (+ (math.abs (- x1 x2))
       (math.abs (- y1 y2)))))

(fn distance* [char x y]
  (let [(xc yc) (char.position)]
    (+ (math.abs (- x xc))
       (math.abs (- y yc)))))

{:create create
 :distance distance
 :distance* distance*
 }

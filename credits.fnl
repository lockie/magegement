(local fonts (require :fonts))

(fn credits [window]
  (local text-color (vec4 1 1 1 1))
  (var credits-mode true)
  (set window.scene
       (^
        (am.group)
        [(^
          (am.translate 0 200)
          (am.text
           fonts.immortal18
           "Thanks for playing!"
           text-color))
         (^
          (am.translate 0 -40)
          (am.text
           fonts.immortal18
           "
PROGRAMMING AND GAME DESIGN
Andrew Kravchuk

ADDITIONAL PROGRAMMING
Fennel programming language by Calvin Rose
Amulet toolkit by Ian MacLarty
Lume library by rxi

ART
Immortal font by Apostrophic Labs
DawnLike tileset by Andrew Rios
Death sounds by Lukasz Jasinski
Sound effects by Little Robot Sound Factory
Scream sound by Brian Johnson
Win jingle by Fupi
Pyromaniac Orc by Merry Dream Games
Monster sounds by pauliuw
"
           text-color))]))
  (window.scene:action (am.play "sounds/win.ogg")))

(local lume (require "contrib.lume"))

(local character (require :character))
(local globals (require :globals))

(local list [])

(fn create [window map player data]
  (local char (character.create window map data))
  (local char-scene-node (char.scene-node))
  (local vision-range (or data.vision-range 10))
  (local damage (or data.damage 3))
  (local attack-range (or data.attack-range 2))
  (local attack-speed (or data.attack-speed 0.5))
  (var time-counter 0)
  (var attack-sound false)
  (var did-drop false)

  (let [tile-size globals.tile-size
        pixel-scale globals.pixel-scale
        scaled-tile-size (* pixel-scale tile-size)]

    (fn attack [target]
      (when (target.alive?)
        (let [(x y) (target.position)
              (my-x my-y) (char.position)]
          (if (< (lume.distance x y my-x my-y true) attack-range)
              (do
                (when (not attack-sound)
                  (char-scene-node:action (am.play (.. "sounds/" data.name "-attack.ogg")))
                  (set attack-sound true))
                (set time-counter (+ time-counter am.delta_time))
                (when (> time-counter attack-speed)
                  (set time-counter (% time-counter attack-speed))
                  (set attack-sound false)
                  (char.stop)
                  (target.set-hp (- (target.hp) damage))))
              (do
                (char.set-target x y)
                (set time-counter 0)
                (set attack-sound false))))))

    (fn action [scene]
      (when (and (char.alive?)
                 (< (character.distance player char) vision-range))
        (attack player))
      (when (and (not (char.alive?))
                 (not did-drop))
        ;; HACK : no time to do proper potion drop
        (set did-drop true)
        (when (= data.name "demon")
          (window.scene:action (am.play "sounds/mana.ogg"))
          (player.set-mana (lume.clamp (+ (player.mana) 4) 0 (player.max-mana))))
        (when (= data.name "ghost")
          (window.scene:action (am.play "sounds/mana.ogg"))
          (player.set-mana (lume.clamp (+ (player.mana) 1) 0 (player.max-mana))))))

    (fn scene-node []
      (: char-scene-node :action :controller action))

    (let [result
          {:stop char.stop
           :scene-node scene-node
           :position char.position
           :set-target char.set-target
           :hp char.hp
           :set-hp char.set-hp
           :alive? char.alive?
           :character (fn [] char)
           :controller action
           :attack attack}]
      (table.insert list result)
      result)))

{:create create
 :list (fn [] list)
 :clear (fn [] (lume.clear list))
 }

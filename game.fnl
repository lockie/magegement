(local lume (require "contrib.lume"))

(local map (require :map))
(local player (require :player))
(local mob (require :mob))

(fn start-game [window show-credits]
  (var level 0)

  (fn load-next-level []
    (fn load-level []
      (if (= level 5)
          (show-credits window)
          (do
            (mob.clear)
            (local m (map.load (.. :level (tostring level))))
            (local objects (m.objects))
            (local player (player.create
                           window m load-next-level
                           (lume.first (lume.filter objects (fn [obj] (= obj.type "player"))))))
            (local mobs (lume.map (lume.filter objects (fn [obj] (= obj.type "mob")))
                                  (lume.fn mob.create window m player)))
            (set window.scene (^ (am.translate (- (/ window.width 2))
                                               (+ (/ window.height 2)))
                                 (am.group
                                  (lume.concat
                                   [(m.scene-node)]
                                   (lume.map mobs (fn [m] (m.scene-node)))
                                   [(player.scene-node)]))))
            (window.scene:action (fn [] (when (window:key_pressed "r") (load-level)))))))

    (set level (+ 1 level))
    (load-level))

  (load-next-level))

(local lume (require "contrib.lume"))

(local globals (require :globals))

(fn load [filename]
  (let [map (require (.. "maps." filename))
        tilesets map.tilesets
        layers map.layers
        tile-size globals.tile-size
        pixel-scale globals.pixel-scale
        tiles {}
        properties {}]
    (each [_ tileset (ipairs tilesets)]
      (let [filename (lume.split tileset.image "/")
            texture (am.texture2d (.. "images/" (. filename (# filename))))
            columns tileset.columns
            rows (/ tileset.imageheight tile-size)
            columns-recipr (/ 1 columns)
            rows-recipr (/ 1 rows)]
        (for [i 0 (- tileset.tilecount 1)]
          (let [index (+ i tileset.firstgid)
                column (% i columns)
                row (math.floor (/ i columns))]
            (tset tiles index
                  {:texture texture
                   :x (/ column columns)
                   :y (/ row rows)
                   :w columns-recipr
                   :h rows-recipr})))
        (each [_ tile (ipairs tileset.tiles)]
          (table.insert properties (+ tile.id tileset.firstgid) tile.properties))))

    (fn scene-node []
      (let [scaled-tile-size (* pixel-scale tile-size)
            nodes {}]
        (each [_ layer (ipairs layers)]
          (when layer.data
            (each [index tile-index (ipairs layer.data)]
              (when (not (= 0 tile-index))
                (let [tile (. tiles tile-index)
                      x (% (- index 1) layer.width)
                      y (math.floor (/ (- index 1) layer.width))]
                  (table.insert
                   nodes
                   (^
                    (am.translate
                     (* scaled-tile-size (+ 0.5 x))
                     (- (* scaled-tile-size (+ 0.5 y))))
                    (am.scale pixel-scale)
                    (am.sprite {:texture tile.texture
                                :s1 tile.x :t1 (- 1 tile.y tile.h)
                                :s2 (+ tile.x tile.w) :t2 (- 1 tile.y)
                                :x1 0 :y1 0 :x2 tile-size :y2 tile-size
                                :width tile-size :height tile-size}))))))))
        (am.group nodes)))

    (fn objects []
      (local result {})
      (each [_ object-layer (ipairs (lume.filter layers (fn [layer] (= layer.type "objectgroup"))))]
        (each [_ object (ipairs object-layer.objects)]
          (table.insert result
                        (lume.merge
                         {:x object.x :y object.y :type object.type :name object.name}
                         (or object.properties {})))))
      result)

    (fn has-property? [x y property]
      (lume.any
       (lume.map
        layers
        (fn [layer]
          (when layer.data
            (let [tile-id (. layer.data (+ (lume.round x) 1 (* (lume.round y) layer.width)))
                  tile-properties (. properties tile-id)]
              (when tile-properties
                (. tile-properties property))))))))

    (fn obstacle? [x y]
      (has-property? x y :obstacle))

    (fn trap? [x y]
      (has-property? x y :trap))

    (fn hole? [x y]
      (has-property? x y :hole))

    (fn exit? [x y]
      (has-property? x y :exit))

    {:scene-node scene-node
     :objects objects
     :obstacle? obstacle?
     :trap? trap?
     :hole? hole?
     :exit? exit?
     }))

{:load
 load}
